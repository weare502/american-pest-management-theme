<?php
/**
 * Template Name: Fight the Bite
 */

$context = Timber::get_context();
$post = new TimberPost();
$post->thumbnail = $post->get_thumbnail();
$context['post'] = $post;
Timber::render( 'fight-the-bite.twig', $context );