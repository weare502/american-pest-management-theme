<?php
/**
 * Template Name: Home Services
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

// get service area info from about us page to reuse here
$about_id = 46;
$context['about_map_header'] = get_field( 'service_area_header', $about_id );
$context['about_map_paragraph'] = get_field( 'service_area_paragraph', $about_id );
$context['about_service_map'] = get_field( 'service_area_map', $about_id );

$context['pests'] = Timber::get_posts( array( 'post_type' => 'pest', 'posts_per_page'=> -1, 'order' => 'ASC', 'orderby' => 'date' ) );
$context['pestmatrix'] = array();

foreach ( $context['pests'] as $pest ){
	$context['pestmatrix'][$pest->slug] = $pest->lowest_package_tier;
	if ( ! $pest->lowest_package_tier ) {
		$context['pestmatrix'][$pest->slug] = 'bronze';
	}
}

$templates = array( 'home-services.twig' );
Timber::render( $templates, $context );