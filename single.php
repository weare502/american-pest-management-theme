<?php
/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$term = Timber::get_term('offices');

$context['cities'] = Timber::get_posts( array( 'post_type' => 'addl_areas', 'posts_per_page' => -1, 'orderby' => 'title', 'order' => 'ASC' ));
$context['home_offices'] = Timber::get_terms([ 'taxonomies' => 'offices' ]);

$terms_arr = get_the_terms( $post->ID, 'offices' );

if( is_array($terms_arr) || is_object($terms_arr)) {
	foreach( $terms_arr as $term ) {
		$termid = $term->term_id;
		$term_link = $term->slug;
		$office_state = get_field( 'full_state', 'offices_' . $termid );
		$office_addx = get_field( 'city_address', 'offices_' . $termid );
		$office_zip = get_field( 'zip_code', 'offices_' . $termid );
		$office_name = get_field( 'city_name', 'offices_' . $termid );
		$office_phone = get_field( 'city_phone_number', 'offices_' . $termid );
	}
}

$context['home_office_state'] = $office_state;
$context['home_office_name'] = $office_name;
$context['home_office_addx'] = $office_addx;
$context['home_office_phone'] = $office_phone;
$context['home_office_slug'] = $term_link;
$context['home_office_zip'] = $office_zip;


if ( post_password_required( $post->ID ) ) {
	Timber::render( 'single-password.twig', $context );
} else {
	Timber::render( array( 'single-' . $post->ID . '.twig', 'single-' . $post->post_type . '.twig', 'single.twig' ), $context );
}