<?php
/**
 * Template for all the Home Office Taxonomies
 * This template pulls the master document from the [Options Page -> "Service Areas Template"]
 *
 *
  * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

$context = Timber::get_context();
$term = Timber::get_term('offices');
$context['term'] = $term;

// used to build the child link list
$context['cities'] = Timber::get_posts( array( 'post_type' => 'addl_areas', 'posts_per_page' => -1, 'orderby' => 'title', 'order' => 'ASC' ));

$templates = array( 'taxonomy-offices.twig' );

Timber::render( $templates, $context );