<?php
/**
 * Template Name: Pest Page Template
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$context['faqs'] = get_field('faqs_section');

$templates = array( 'pest-page.twig' );

Timber::render( $templates, $context );