<?php
/**
 * Template Name: Pest Perks
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$context['referral_form'] = gravity_form( 14, false, false, false, '', false, 3, false );
// PARAMS for gravity_form() function
// gravity_form( $id_or_title, $display_title = true, $display_description = true, $display_inactive = false, $field_values = null, $ajax = false, $tabindex, $echo = true );

$templates = array( 'pest-perks.twig' );

Timber::render( $templates, $context );