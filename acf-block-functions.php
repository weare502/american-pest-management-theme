<?php
// This functions file is for all custom blocks added via ACF
// Reference: https://www.advancedcustomfields.com/resources/acf_register_block_type/

if( function_exists('acf_register_block_type') ) :
	include 'acf-blocks-callback.php'; // pass-off to let Timber render the blocks

	$dropdown_block = [
		'name' => 'dropdown-block',
		'title' => __( 'Dropdown Block', 'apm' ),
		'description' => __( 'Creates a dropdown container; The content is folded into the dropdown title.', 'apm' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'apm-blocks',
		'align' => 'wide',
		'icon' => 'sort',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'drop', 'down', 'dropdown', 'accordion' ]
	];
	acf_register_block_type( $dropdown_block );

	$important_info_block = [
		'name' => 'important-info-block',
		'title' => __( 'Important Info', 'apm' ),
		'description' => __( 'Creates an important message "banner" with a dark blue left border, and text on the right.', 'apm' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'apm-blocks',
		'align' => 'center',
		'icon' => 'info',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'important', 'info', 'message', 'blue', 'dark' ]
	];
	acf_register_block_type( $important_info_block );

	$blue_button_block = [
		'name' => 'blue-button-block',
		'title' => __( 'Blue Button', 'apm' ),
		'description' => __( 'Creates a blue button with white text and arrow.', 'apm' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'apm-blocks',
		'align' => 'wide',
		'icon' => 'editor-removeformatting',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'button', 'anchor', 'blue' ]
	];
	acf_register_block_type( $blue_button_block );

	$white_button_block = [
		'name' => 'white-button-block',
		'title' => __( 'White Button', 'apm' ),
		'description' => __( 'Creates a white button with blue text and arrow.', 'apm' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'apm-blocks',
		'align' => 'wide',
		'icon' => 'editor-removeformatting',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'button', 'anchor', 'white' ]
	];
	acf_register_block_type( $white_button_block );

	$multi_column_grid = [
		'name' => 'multi-column-grid',
		'title' => __( 'Multi-Column Grid', 'apm' ),
		'description' => __( 'Creates a Grid with 1, 2, or 3 columns; centered.', 'apm' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'apm-blocks',
		'align' => 'wide',
		'icon' => 'layout',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'grid', 'multi', 'multiple', 'column', '1', '2', '3' ]
	];
	acf_register_block_type( $multi_column_grid );

	$full_width_block = [
		'name' => 'full-width-block',
		'title' => __( 'Full-Width Content Block', 'apm' ),
		'description' => __( 'Creates a block with 2 staggered images on the left, and text content on the right.', 'apm' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'apm-blocks',
		'align' => 'full',
		'icon' => 'editor-expand',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'full', 'width', 'block', 'interstitial' ]
	];
	acf_register_block_type( $full_width_block );

	$refer_friend_block = [
		'name' => 'refer-friend-block',
		'title' => __( 'Refer a Friend Block', 'apm' ),
		'description' => __( 'Creates a block with content from the "Refer a Friend" block setup on the Global Options Page (consistent content across all pages).', 'apm' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'apm-blocks',
		'align' => 'wide',
		'icon' => 'rest-api',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'refer', 'a', 'friend', 'block', 'spinner', 'step', 'reward' ]
	];
	acf_register_block_type( $refer_friend_block );
	
	$text_image_content = [
		'name' => 'text-image-content',
		'title' => __( 'Text/Image Content', 'apm' ),
		'description' => __( 'Creates a block with text content to the left and an image on the right.', 'apm' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'apm-blocks',
		'align' => 'wide',
		'icon' => 'welcome-widgets-menus',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'image', 'text', 'content', 'split' ]
	];
	acf_register_block_type( $text_image_content );

	$pest_header_block = [
		'name' => 'pest-header-block',
		'title' => __( 'Header Intro Block', 'apm' ),
		'description' => __( 'Creates a block with the page title and text content to the left, and the featured image to the right.', 'apm' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'apm-blocks',
		'align' => 'wide',
		'icon' => 'welcome-widgets-menus',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'image', 'text', 'content', 'split', 'header', 'title' ]
	];
	acf_register_block_type( $pest_header_block );
endif;