<?php
/**
 * Template Name: About Us
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$templates = array( 'about-us.twig' );

Timber::render( $templates, $context );