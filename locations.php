<?php
/**
 * Template Name: Locations
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$context['cities'] = Timber::get_posts( array(
	'post_type' => 'addl_areas',
	'posts_per_page' => -1,
));

// timber way of getting custom taxonomies
$context['office_locations'] = Timber::get_terms(['taxonomies' => 'offices', 'orderby' => 'menu_order']);
the_post();

$templates = array( 'locations.twig' );

Timber::render( $templates, $context );