<?php
/**
 * Template Name: Pest Library
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$post = new TimberPost();
$post->thumbnail = $post->get_thumbnail();
$context['post'] = $post;
$context['pests'] = Timber::get_posts( array( 'post_type' => 'pest', 'posts_per_page' => 1000 ) );
$context['videos'] = Timber::get_posts( array( 'post_type' => 'video', 'posts_per_page' => 1000 ) );
$context['featured_posts'] = Timber::get_posts( array( 'post_type' => 'post', 'posts_per_page' => 3, 'cat' => 5 ) );
$context['top_featured_post'] = array_shift( $context['featured_posts'] );

Timber::render( 'pest-library.twig', $context );