/* jshint devel: true */
/* jshint ignore:start */
/**
 * String.startsWith shim
 */
if (!String.prototype.startsWith) {
		String.prototype.startsWith = function(searchString, position){
			position = position || 0;
			return this.substr(position, searchString.length) === searchString;
	};
}

/**
 * FitVids JS
 */
;(function( $ ){

	'use strict';

	$.fn.fitVids = function( options ) {
		var settings = {
			customSelector: null,
			ignore: null
		};

		if(!document.getElementById('fit-vids-style')) {
			// appendStyles: https://github.com/toddmotto/fluidvids/blob/master/dist/fluidvids.js
			var head = document.head || document.getElementsByTagName('head')[0];
			var css = '.fluid-width-video-wrapper{width:100%;position:relative;padding:0;}.fluid-width-video-wrapper iframe,.fluid-width-video-wrapper object,.fluid-width-video-wrapper embed {position:absolute;top:0;left:0;width:100%;height:100%;}';
			var div = document.createElement("div");
			div.innerHTML = '<p>x</p><style id="fit-vids-style">' + css + '</style>';
			head.appendChild(div.childNodes[1]);
		}

		if ( options ) {
			$.extend( settings, options );
		}

		return this.each(function(){
			var selectors = [
				'iframe[src*="player.vimeo.com"]',
				'iframe[src*="youtube.com"]',
				'iframe[src*="youtube-nocookie.com"]',
				'iframe[src*="kickstarter.com"][src*="video.html"]',
				'object',
				'embed'
			];

			if (settings.customSelector) {
				selectors.push(settings.customSelector);
			}

			var ignoreList = '.fitvidsignore';

			if(settings.ignore) {
				ignoreList = ignoreList + ', ' + settings.ignore;
			}

			var $allVideos = $(this).find(selectors.join(','));
			$allVideos = $allVideos.not('object object'); // SwfObj conflict patch
			$allVideos = $allVideos.not(ignoreList); // Disable FitVids on this video.

			$allVideos.each(function(){
				var $this = $(this);
				if($this.parents(ignoreList).length > 0) {
					return; // Disable FitVids on this video.
				}
				if (this.tagName.toLowerCase() === 'embed' && $this.parent('object').length || $this.parent('.fluid-width-video-wrapper').length) { return; }
				if ((!$this.css('height') && !$this.css('width')) && (isNaN($this.attr('height')) || isNaN($this.attr('width'))))
				{
					$this.attr('height', 9);
					$this.attr('width', 16);
				}
				var height = ( this.tagName.toLowerCase() === 'object' || ($this.attr('height') && !isNaN(parseInt($this.attr('height'), 10))) ) ? parseInt($this.attr('height'), 10) : $this.height(),
						width = !isNaN(parseInt($this.attr('width'), 10)) ? parseInt($this.attr('width'), 10) : $this.width(),
						aspectRatio = height / width;
				if(!$this.attr('name')){
					var videoName = 'fitvid' + $.fn.fitVids._count;
					$this.attr('name', videoName);
					$.fn.fitVids._count++;
				}
				$this.wrap('<div class="fluid-width-video-wrapper"></div>').parent('.fluid-width-video-wrapper').css('padding-top', (aspectRatio * 100)+'%');
				$this.removeAttr('height').removeAttr('width');
			});
		});
	};
	
	// Internal counter for unique video names.
	$.fn.fitVids._count = 0;

// Works with either jQuery or Zepto
})( window.jQuery || window.Zepto );
/* jshint ignore:end */

jQuery( document ).ready( function( $ ) {
	var $body = $('body');
	$('body').fitVids();


	// mobile menu toggle effect and body lock
	$('#menu-toggle').click( function() {
		$('.x-bar').toggleClass('x-bar-active');
		$('.nav-menu').toggleClass('show-menu');
		$('#primary-menu').fadeToggle(550);
		$('html, body').toggleClass('body-lock');
	});

	$('li > .sub-menu > .menu-item-has-children').hover(
		function() {
			$(this).parent().addClass('border-mod');
		},

		function() {
			$(this).parent().removeClass('border-mod');
		}
	);

	// mobile nav core functions - sub-menu open/expand functions and handlers for back button
	$(function mobileNavigationRouter() {
		if( $body.width() < 1024) {
			// add back button at top of each sub-menu
			$('.sub-menu').prepend('<li><button id="mobile-back-button" class="menu-back"> Back</button></li>');

			// close sub-menu when back button is clicked
			$('li button.menu-back').on('click', function(e) {
				var $menu = $(this).parents('.sub-menu:first');
				$menu.removeClass('sub-menu-open');
				e.stopPropagation(); // prevent event from bubbling back up
			});

			// expand function
			$('.menu-item-has-children').click(function(e) {
				var $sub = $(this).find('.sub-menu:first');

				// if it has children, but doesn't have it's menu open, do not follow the link
				if( ! $sub.hasClass('sub-menu-open')) {
					e.preventDefault(); // don't follow links on first click
				}

				$sub.addClass('sub-menu-open');
				e.stopPropagation(); // prevent from bubbling back up
			});
		}
	});

	$('.open-popup-link').magnificPopup({
		type:'inline',
		removalDelay: 300,
		midClick: true // Allow opening popup on middle mouse click.
	});

	$('.magnific-trigger').magnificPopup({
		type:'inline',
		removalDelay: 300,
		midClick: true
	});

	if ( $.fn.slick ){
		$('.inline-video-gallery').slick({
			slidesToScroll: 1, slidesToShow: 3, 
			// infinite: true, 
			// centerMode: true,
			focusOnSelect: true,
			prevArrow: '<span class="fa fa-chevron-left left"></span>',
			nextArrow: '<span class="fa fa-chevron-right right"></span>',
			responsive: [
			    {
			    	breakpoint: 1000,
			    	settings: {
						centerMode: true,
			    	}
			    }
			]
		});
	}

	if ( $body.hasClass('page-template-pest-library') ){
		var pests = $('.pests-grid .label').slice(8);
		$(pests).hide();
		$('.pests-grid').after('<p class="centered-text top-bottom-padding"><button id="showAllPests" class="button">Show All Pests</button></p>');
		$('#showAllPests').click(function(){
			$(pests).show();
			$('#showAllPests').hide();
		});
	}

	$('.banner-close-button').click(function() {
		$('.covid-banner').hide();
	});
}); // End Document Ready