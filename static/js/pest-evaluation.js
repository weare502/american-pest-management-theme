/* global Vue, console, CryptoJS, _ */
/* exported evaluationApp */

// @codekit-prepend "../../bower_components/jquery.maskedinput/dist/jquery.maskedinput.min.js";

/* jshint ignore:start */
/*
    jQuery Masked Input Plugin
    Copyright (c) 2007 - 2015 Josh Bush (digitalbush.com)
    Licensed under the MIT license (http://digitalbush.com/projects/masked-input-plugin/#license)
    Version: 1.4.1
*/
!function(a){"function"==typeof define&&define.amd?define(["jquery"],a):a("object"==typeof exports?require("jquery"):jQuery)}(function(a){var b,c=navigator.userAgent,d=/iphone/i.test(c),e=/chrome/i.test(c),f=/android/i.test(c);a.mask={definitions:{9:"[0-9]",a:"[A-Za-z]","*":"[A-Za-z0-9]"},autoclear:!0,dataName:"rawMaskFn",placeholder:"_"},a.fn.extend({caret:function(a,b){var c;if(0!==this.length&&!this.is(":hidden"))return"number"==typeof a?(b="number"==typeof b?b:a,this.each(function(){this.setSelectionRange?this.setSelectionRange(a,b):this.createTextRange&&(c=this.createTextRange(),c.collapse(!0),c.moveEnd("character",b),c.moveStart("character",a),c.select())})):(this[0].setSelectionRange?(a=this[0].selectionStart,b=this[0].selectionEnd):document.selection&&document.selection.createRange&&(c=document.selection.createRange(),a=0-c.duplicate().moveStart("character",-1e5),b=a+c.text.length),{begin:a,end:b})},unmask:function(){return this.trigger("unmask")},mask:function(c,g){var h,i,j,k,l,m,n,o;if(!c&&this.length>0){h=a(this[0]);var p=h.data(a.mask.dataName);return p?p():void 0}return g=a.extend({autoclear:a.mask.autoclear,placeholder:a.mask.placeholder,completed:null},g),i=a.mask.definitions,j=[],k=n=c.length,l=null,a.each(c.split(""),function(a,b){"?"==b?(n--,k=a):i[b]?(j.push(new RegExp(i[b])),null===l&&(l=j.length-1),k>a&&(m=j.length-1)):j.push(null)}),this.trigger("unmask").each(function(){function h(){if(g.completed){for(var a=l;m>=a;a++)if(j[a]&&C[a]===p(a))return;g.completed.call(B)}}function p(a){return g.placeholder.charAt(a<g.placeholder.length?a:0)}function q(a){for(;++a<n&&!j[a];);return a}function r(a){for(;--a>=0&&!j[a];);return a}function s(a,b){var c,d;if(!(0>a)){for(c=a,d=q(b);n>c;c++)if(j[c]){if(!(n>d&&j[c].test(C[d])))break;C[c]=C[d],C[d]=p(d),d=q(d)}z(),B.caret(Math.max(l,a))}}function t(a){var b,c,d,e;for(b=a,c=p(a);n>b;b++)if(j[b]){if(d=q(b),e=C[b],C[b]=c,!(n>d&&j[d].test(e)))break;c=e}}function u(){var a=B.val(),b=B.caret();if(o&&o.length&&o.length>a.length){for(A(!0);b.begin>0&&!j[b.begin-1];)b.begin--;if(0===b.begin)for(;b.begin<l&&!j[b.begin];)b.begin++;B.caret(b.begin,b.begin)}else{for(A(!0);b.begin<n&&!j[b.begin];)b.begin++;B.caret(b.begin,b.begin)}h()}function v(){A(),B.val()!=E&&B.change()}function w(a){if(!B.prop("readonly")){var b,c,e,f=a.which||a.keyCode;o=B.val(),8===f||46===f||d&&127===f?(b=B.caret(),c=b.begin,e=b.end,e-c===0&&(c=46!==f?r(c):e=q(c-1),e=46===f?q(e):e),y(c,e),s(c,e-1),a.preventDefault()):13===f?v.call(this,a):27===f&&(B.val(E),B.caret(0,A()),a.preventDefault())}}function x(b){if(!B.prop("readonly")){var c,d,e,g=b.which||b.keyCode,i=B.caret();if(!(b.ctrlKey||b.altKey||b.metaKey||32>g)&&g&&13!==g){if(i.end-i.begin!==0&&(y(i.begin,i.end),s(i.begin,i.end-1)),c=q(i.begin-1),n>c&&(d=String.fromCharCode(g),j[c].test(d))){if(t(c),C[c]=d,z(),e=q(c),f){var k=function(){a.proxy(a.fn.caret,B,e)()};setTimeout(k,0)}else B.caret(e);i.begin<=m&&h()}b.preventDefault()}}}function y(a,b){var c;for(c=a;b>c&&n>c;c++)j[c]&&(C[c]=p(c))}function z(){B.val(C.join(""))}function A(a){var b,c,d,e=B.val(),f=-1;for(b=0,d=0;n>b;b++)if(j[b]){for(C[b]=p(b);d++<e.length;)if(c=e.charAt(d-1),j[b].test(c)){C[b]=c,f=b;break}if(d>e.length){y(b+1,n);break}}else C[b]===e.charAt(d)&&d++,k>b&&(f=b);return a?z():k>f+1?g.autoclear||C.join("")===D?(B.val()&&B.val(""),y(0,n)):z():(z(),B.val(B.val().substring(0,f+1))),k?b:l}var B=a(this),C=a.map(c.split(""),function(a,b){return"?"!=a?i[a]?p(b):a:void 0}),D=C.join(""),E=B.val();B.data(a.mask.dataName,function(){return a.map(C,function(a,b){return j[b]&&a!=p(b)?a:null}).join("")}),B.one("unmask",function(){B.off(".mask").removeData(a.mask.dataName)}).on("focus.mask",function(){if(!B.prop("readonly")){clearTimeout(b);var a;E=B.val(),a=A(),b=setTimeout(function(){B.get(0)===document.activeElement&&(z(),a==c.replace("?","").length?B.caret(0,a):B.caret(a))},10)}}).on("blur.mask",v).on("keydown.mask",w).on("keypress.mask",x).on("input.mask paste.mask",function(){B.prop("readonly")||setTimeout(function(){var a=A(!0);B.caret(a),h()},0)}),e&&f&&B.off("input.mask").on("input.mask",u),A()})}})});
/* jshint ignore:end */


function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}

Vue.directive('mask', function (el, binding) {
	jQuery(el).mask(binding.value);
	console.log(el, binding);
});

var datePickerStartDate = function(){
	var d = new Date();
	var r = "2";
	switch ( d.getDay() ){
		case 0:
			// Sunday to Wednesday
			r = "3";
			break;
		case 4:
			// Thursday to Tuesday
			r = "4";
			break;
		case 5:
			// Friday to Wednesday
			r = "5";
			break;
		case 6:
			// Saturday to Wednesday
			r = "4";
			break;
		default:
			break;
	}
	return r;
};

Vue.component('datepicker', {
	props: ['value'],
	template: '<input type="text" ref="input" v-bind:value="value" v-on:input="$emit(\'input\', $event.target.value)" @blur="validateField"/>',
	mounted: function () {
	    // activate the plugin when the component is mounted.
	    jQuery(this.$el).datepicker({
	    	beforeShowDay: jQuery.datepicker.noWeekends,
	        dateFormat: 'M d, yy',
	        minDate: datePickerStartDate(),
	        maxDate: "+14d",
	        onClose: this.onClose
	    });
	},

	methods: {
	    // callback for when the selector popup is closed.
	    onClose: function(date) {
	        this.$emit('input', date);
	        this.$el.blur();
	    },
	    validateField: function(event){
	    	var el = event.target;
	    	var $el = jQuery(el);
	    	if ( el.checkValidity() ){
	    		$el.addClass('is-success');
	    		$el.removeClass('is-danger');
	    	} else {
	    		$el.removeClass('is-success');
	    		$el.addClass('is-danger');
	    	}
	    }
	},
	watch: {
	    // when the value for the input is changed from the parent,
	    // the value prop will update, and we pass that updated value to the plugin.
	    value: function(newVal) {
	    	jQuery(this.el).datepicker('setDate', newVal); 
	    }
	}
});

var evaluationApp = new Vue({
	el: '#vue-wrapper',
	delimiters: ['${', '}'],
	data: {
		homestep: 0,
		bizstep: 0,
		form: {
			firstName: '',
			lastName: '',
			streetAddress: '',
			city: '',
			zip: '',
			phone: '',
			email: '',
			currentPests: [],

			bedroomCount: '',
			storiesCount: '',
			prog: '',

			industryType: '',
			businessName: '',

			program: '',
			time: '',
			date: '',
			startTime: '',
			endTime: '',
			startAmPm: 'AM',
			endAmPm: 'AM',
			additionalInformation: '',
			programDetailsHTML: '',
			programNameHTML: '',
			referralType: '',
			promo: '',
		},
		showError: false,
		pestMatrix: {},
		submitting: false,
	},

	methods: {
		// start Home Steps Logic
		homeNextStep: function() {
			var valid = true;
			this.showError = false;

			jQuery('.current-step form input').each(function(i, el) {
				if ( ! el.checkValidity() ) {
					valid = false;
				}
			});

			if( this.homestep === 2 ) {
				valid = this.form.currentPests.length ? true : false;
				if (valid){
					this.homestep += 1;
				}
			}

			if( this.homestep === 3 ) {

				if(valid){
					valid = this.form.time !== '' && this.form.date !== '';
				}

				if(valid) {
					this.homestep += 1;
					this.$nextTick(function() {
						this.form.programDetailsHTML = jQuery('.pest-program .program-details')[0].innerHTML || '';
						this.form.programNameHTML = jQuery('.pest-program .program-name')[0].outerHTML || '';
						this.homeNextStep();
					});
					return;
				} else {
					this.showError = true;
				}
				return;
			}

			if( this.homestep === 3 ) {
				this.form.program = this.lowestTier;
				valid = this.form.program !== '' ? true : false;
				this.$nextTick(function() {
					this.homestep += 1;
				});
			}

			if(valid) {
				this.homestep += 1;
			} else {
				this.showError = true;
			}
		},

		homePreviousStep: function() {
			this.showError = false;
			this.homestep -= 1;
		},

		// start Business Steps Logic
		bizNextStep: function() {
			var valid = true;
			this.showError = false;

			jQuery('.current-step form input').each(function(i, el){
				if ( ! el.checkValidity() ){
					valid = false;
				}
			});

			if( this.bizstep === 2 ) {
				valid = this.form.currentPests.length ? true : false;
				if (valid){
					this.bizstep += 1;
				}
			}

			if( this.bizstep === 3 ) {
				if ( valid ) {
					valid = this.form.time !== '' && this.form.date !== '';
				}
				return;
			}

			if( this.bizstep === 4 ) {
				this.$nextTick(function() {
					this.bizstep += 1;
				});
				return;
			}

			if(valid) {
				this.bizstep += 1;
			} else {
				this.showError = true;
			}
		},

		bizPreviousStep: function() {
			this.showError = false;
			this.bizstep -= 1;
		},

		isJson: function(item) {
		    item = typeof item !== "string" ? JSON.stringify(item) : item;
		    try {
		        item = JSON.parse(item);
		    } catch (e) {
		        return false;
		    }
		    if (typeof item === "object" && item !== null) {
		        return true;
		    }
		    return false;
		},

		updateLocalStorage: function() {
			window.localStorage.setItem('pestEvaluationData', JSON.stringify(this.form));
		},

		retrieveLocalStorage: function() {
			if ( window.localStorage.getItem('pestEvaluationData') && this.isJson( window.localStorage.getItem('pestEvaluationData') ) ){
				this.form = Object.assign(this.form, JSON.parse(window.localStorage.getItem('pestEvaluationData')));
			} else {
				this.clearLocalStorage();
			}
		},

		clearLocalStorage: function() {
			window.localStorage.removeItem('pestEvaluationData');
			var email = this.form.email;
			this.form = JSON.parse(window.localStorage.getItem('pestEvaluationEmptyForm'));
			this.form.email = email;
		},

		formatPhoneNumber: function( event ) {
			var separator = '-';
			var number = this.form.phone.trim();
			if ( 10 === number.length ){
				number = number.replace( /[^\d]/g, '' ).replace( /(\d{3})(\d{3})(\d{4})/, '$1' + separator + '$2' + separator + '$3' );
			} else if ( 7 === number.length ){
				number = number.replace( /[^\d]/g, '' ).replace( /(\d{3})(\d{4})/, '$1' + separator + '$2' );
			}
			this.form.phone = number;
			this.validateField(event);
		},

		validateField: function(event){
			var el = event.target;
			var $el = jQuery(el);
			if ( el.checkValidity() ){
				$el.addClass('is-success');
				$el.removeClass('is-danger');
			} else {
				$el.removeClass('is-success');
				$el.addClass('is-danger');
			}
		},

		selectProgram: function( program ){
			this.form.program = program;
			this.$nextTick(function(){
				jQuery('body').animate({
					scrollTop: jQuery('#form-top').offset().top - 100
				}, 1000);
			});
		},

		CalculateSig: function(stringToSign, privateKey){
			//calculate the signature needed for authentication
			var hash = CryptoJS.HmacSHA1(stringToSign, privateKey);
			var base64 = hash.toString(CryptoJS.enc.Base64);
			return encodeURIComponent(base64);
		},

		submitForm: function(){
			this.submitting = true;

			//set variables
			var d = new Date();
			var expiration = 3600; // 1 hour,
			var unixtime = parseInt(d.getTime() / 1000);
			var future_unixtime = unixtime + expiration;
			var publicKey = "0f5eba7898";
			var privateKey = "d148896ff123af6";
			var method = "POST";
			var route = "forms/1/submissions";

			var stringToSign = publicKey + ":" + method + ":" + route + ":" + future_unixtime;
			var sig = this.CalculateSig(stringToSign, privateKey);
			var url = '/gravityformsapi/' + route + '?api_key=' + publicKey + '&signature=' + sig + '&expires=' + future_unixtime;

			// Laid out based on how it is emailed to the client
			// The same form is used for Business and Home service requests - empty fields are not sent
			var values = {
				input_values : {
					"input_1": this.form.firstName + ' ' + this.form.lastName,
					"input_15": this.form.businessName,
					"input_14": this.form.industryType,
					"input_2_1": this.form.streetAddress,
					"input_2_3": this.form.city,
					"input_2_5": this.form.zip,
					"input_3": this.form.phone,
					"input_4": this.form.email,
					"input_5": this.form.currentPests,
					"input_7": jQuery('#formprog').val(),
					"input_8": this.form.date,
					"input_9": this.timeWindow,
					"input_10": this.additionalInformation,
					"input_12": this.form.bedroomCount,
					"input_13": this.form.storiesCount,
					"input_17": this.form.referralType,
					"input_18": this.form.promo,
				}
			};

			//json encode array
			var values_json = JSON.stringify(values);
			var that = this;
			jQuery.post(url, values_json, function(data){
			    that.submitting = false;
			    if ( data.response.is_valid ){
					that.homestep = 7;
					// that.bizstep = 7;
					window.location.href = '/thank-you';
					that.clearLocalStorage();
			    }
			});
		}
	},

	created: function(){
		window.localStorage.setItem('pestEvaluationEmptyForm', JSON.stringify(this.form) );
		this.retrieveLocalStorage();
		this.form.firstName = getUrlParameter('firstname');
		this.form.lastName = getUrlParameter('lastname');
		this.form.email = getUrlParameter('email');
		this.form.zip = getUrlParameter('zip');
		jQuery('input, select').blur();
		this.pestMatrix = window.pestMatrix;
	},

	updated: function(){
		this.updateLocalStorage();
	},

	computed: {
		showFormDate: function(){
			if ( this.form.time === '' || this.form.time === 'custom' || ! this.form.time ){
				return false;
			}
			return true;
		},

		timeWindow: function(){
			return this.form.time.charAt(0).toUpperCase() + this.form.time.slice(1);
		},

		additionalInformation: function(){
			if ( ! this.form.additionalInformation ){
				return 'N/A';
			}
			return this.form.additionalInformation;
		},

		lowestTier: function(){
			var tier = 'bronze';
			var pests = _.map( _.union( this.form.currentPests, this.form.futurePests ), function(pest){
				return pest.toLowerCase().replace(' ', '-');
			} );
			var matrix = _.pick( this.pestMatrix, pests );
			matrix = _.uniq( _.values(matrix) );

			if ( matrix.indexOf('americaschoice') !== -1 ){
				tier = 'americaschoice';
			} else if ( matrix.indexOf('platinum') !== -1 ){
				tier = 'platinum';
			} else if ( matrix.indexOf('gold') !== -1 ){
				tier = 'gold';
			} else if ( matrix.indexOf('silver') !== -1 ){
				tier = 'silver';
			} else if ( matrix.indexOf('bronze') !== -1 ){
				tier = 'bronze';
			} else if ( matrix.indexOf('fightthebite') !== -1 ){
				tier = 'fightthebite';
			} else if ( matrix.indexOf('snakecontrol') !== -1 ){
				tier = 'snakecontrol';
			}
			return tier;
		}
	},

	watch: {
		'form.program': function(newVal){
			if ( 4 !== this.homestep ){
				return;
			}
			if ( newVal !== '' ){
				this.$nextTick(function(){
					this.form.programDetailsHTML = jQuery('.pest-program .program-details')[0].innerHTML || '';
					this.form.programNameHTML = jQuery('.pest-program .program-name')[0].outerHTML || '';
					this.homeNextStep();
				});
			} else {
				this.form.programHTML = '';
			}
		},

		homestep: function(){
			this.$nextTick(function(){
				jQuery('html,body').animate({
					scrollTop: jQuery('#form-top').offset().top - 100
				}, 1000);
				jQuery('input, select').blur();

				// dropdown toggle for Pest Program Details
				jQuery('.pest-program .program-details').hide();
				jQuery('.pest-listing').hide();
				jQuery('.toggle').click(function() {
					jQuery('.dropdown-toggle-container').prev('.program-details').slideToggle(550);
					jQuery('.pest-listing').slideToggle(550);
					if( jQuery(this).hasClass('active') ) {
						jQuery(this).removeClass('active');
						jQuery(this).text('See what\'s included in the program.');
					} else {
						jQuery(this).addClass('active');
						jQuery(this).text('Collapse');
					}
				});
			});
		},

		bizstep: function(){
			this.$nextTick(function(){
				jQuery('html,body').animate({
					scrollTop: jQuery('#form-top').offset().top - 100
				}, 1000);
				jQuery('input, select').blur();
			});
		},
	},

	mounted: function(){
		jQuery('form').on('submit', function(e){
			e.preventDefault();
		});
	},
});

jQuery(".datepicker").datepicker();