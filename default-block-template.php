<?php
/**
 * Template Name: Default Block Template
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
*/

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$templates = array( 'default-block-template.twig' );
Timber::render( $templates, $context );