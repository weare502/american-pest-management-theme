<?php
/**
 * Template Name: Snake Identification Page
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
*/

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$context['faqs'] = get_field('faqs_section');

$templates = array( 'snakes-page.twig' );

Timber::render( $templates, $context );