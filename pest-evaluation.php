<?php
/**
 * Template Name: Pest Evaluation
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$post = new TimberPost();
$post->thumbnail = $post->get_thumbnail();
$context['post'] = $post;
$context['pests'] = Timber::get_posts( array( 'post_type' => 'pest', 'posts_per_page'=> -1, 'order' => 'ASC', 'orderby' => 'title' ) );
$context['pestmatrix'] = array();

foreach ( $context['pests'] as $pest ){
	$context['pestmatrix'][$pest->slug] = $pest->lowest_package_tier;
	if ( ! $pest->lowest_package_tier ){
		$context['pestmatrix'][$pest->slug] = 'bronze';
	}
}

// var_dump($context['pestmatrix']);die();

Timber::render( 'pest-evaluation.twig', $context );