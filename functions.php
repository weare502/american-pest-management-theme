<?php

if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
		echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
	});
	return;
}

Timber::$dirname = array('templates', 'views');

class TwentyFourSevenSite extends TimberSite {

	function __construct() {
		// add_theme_support( 'post-formats' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );
		add_theme_support( 'title-tag' );
		add_theme_support( 'align-wide' );
 
		add_action( 'after_setup_theme', array( $this, 'after_setup_theme' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		add_action( 'admin_head', array( $this, 'admin_head_css' ) );
		add_action( 'init', array( $this, 'register_post_types' ) );
		add_action( 'p2p_init', array( $this, 'p2p_init' ) );
		add_action( 'init', array( $this, 'offices_taxonomy' ), 0);
		add_action( 'acf/init', [ $this, 'render_custom_acf_blocks' ] );
		add_action( 'enqueue_block_assets', [ $this, 'frontend_backend_styles' ] );

		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
		add_filter( 'mce_buttons_2', array( $this, 'tiny_mce_buttons' ) );
		add_filter( 'tiny_mce_before_init', array( $this, 'tiny_mce_insert_formats' ) );
		add_filter( 'dashboard_glance_items', array( $this, 'dashboard_glance_items' ) );
		add_filter( 'block_categories', [ $this, 'apm_block_category' ], 99, 1 );

		// Removed annoying admin bar during dev (breaks header area if added back in)
		add_filter('show_admin_bar', '__return_false');

		add_action( 'init', function(){
			add_editor_style( get_stylesheet_uri() );
		});

		parent::__construct();
	}

	function add_to_context( $context ) {
		$context['site'] = $this;
		$context['logo'] = trailingslashit( get_template_directory_uri() ) . 'static/images/logo.png';
		$context['year'] = date('Y');
		$context['options'] = get_fields('option');
		$context['is_home'] = is_home();
		$context['is_front'] = is_front_page();
		$context['csscache'] = filemtime(get_stylesheet_directory() . '/style.css');
		$context['plugin_content'] = TimberHelper::ob_function( 'the_content' );
		return $context;
	}

	function after_setup_theme() {
		register_nav_menu( 'primary', 'Main Navigation' );
		register_nav_menu( 'quick_links', 'Quick Links' );
		register_nav_menu( 'faq', 'FAQ Links' );
		register_nav_menu( 'secondary', 'Secondary Navigation' );

		// Images Sizes
		add_image_size( 'xlarge', 1500, 1500 );

		// Options page holds the Master Document for the Additional Areas Served
		acf_add_options_page([
			'page_title' 	=> 'Global Site Options',
			'menu_title'	=> 'Global Site Options',
			'menu_slug' 	=> 'apm-site-options',
			'capability'	=> 'edit_posts',
			'redirect'		=> false
		]);

		acf_add_options_sub_page([
			'page_title' => 'Pest Evaluation Options',
			'menu_title' => 'Pest Evaluation Options',
			'parent_slug' => 'apm-site-options'
		]);

		acf_add_options_sub_page([
			'page_title' => 'Service Areas Master Document',
			'menu_title' => '',
			'parent_slug' => 'apm-site-options'

		]);
	}

	// main site styles / scripts
	function enqueue_scripts() {
		wp_enqueue_style( 'magnific-popup-style', get_template_directory_uri() . '/bower_components/magnific-popup/dist/magnific-popup.css', array(), '20190609' );
		wp_enqueue_script( 'magnific-popup', get_template_directory_uri() . '/bower_components/magnific-popup/dist/jquery.magnific-popup.min.js', array( 'jquery' ), '20190609', true );
		wp_enqueue_script( 'apm-theme', get_template_directory_uri() . "/static/js/site.js", array( 'jquery', 'underscore', 'magnific-popup', 'jquery-ui-accordion' ), '25160579' );
		if ( is_page(101) ){
			wp_enqueue_script( 'apm-vue', get_template_directory_uri() . "/static/js/vue.js", array( 'jquery', 'underscore' ), '25190559', true );
			wp_enqueue_script( 'apm-evaluation', get_template_directory_uri() . "/static/js/pest-evaluation.js", array( 'jquery', 'underscore', 'magnific-popup', 'apm-vue', 'jquery-ui-datepicker' ), '25197879', true );
		}
		wp_localize_script( 'magnific-popup', "wpThemeUrl", get_stylesheet_directory_uri() );
		wp_register_style('jquery-ui', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css');
		wp_enqueue_style( 'jquery-ui' );
	}

	// block styles / scripts
	function frontend_backend_styles() {
		wp_enqueue_style( 'blocks-css', get_stylesheet_directory_uri() . '/block-style-dist.css' );
	}

	function admin_head_css() {
		?><style type="text/css">
			#wp-admin-bar-comments { display: none !important; }
			.update-nag { display: none !important; }
			.mce-ico.fa { font-family: 'FontAwesome', 'Dashicons'; }

			/* Hide annoying ManageWP popup */
			.mwp-notice-container { display: none !important; }

			/* Hides the parent drop down on categories */
			.form-field.term-parent-wrap { display: none; }
		</style><?php
	}

	function tiny_mce_buttons( $buttons ) {
		array_unshift( $buttons, 'styleselect' );
		return $buttons;
	}

	// Callback function to filter the MCE settings
	function tiny_mce_insert_formats( $init_array ) {  
		// Define the style_formats array
		$style_formats = array(  
			// Each array child is a format with it's own settings
			array(  
				'title'    => 'Button',  
				'selector' => 'a',  
				'classes'  => 'button',
				// font awesome must be available in the admin area to see the icon
				'icon'     => ' fa fa-hand-pointer-o'
			),
			array(  
				'title'    => 'Red Text',  
				'selector' => '*',  
				'classes'  => 'red-text',
				// font awesome must be available in the admin area to see the icon
				'icon'     => ' fa fa-eye-dropper'
			),
		);  
		// Insert the array, JSON ENCODED, into 'style_formats'
		$init_array['style_formats'] = json_encode( $style_formats );
		$init_array['body_class'] .= " content ";
		// die(var_dump($init_array));
		return $init_array;
	}

	// Home Office Locations Category - We register this before out CPT so the URL rewrite works
	function offices_taxonomy() {
		$labels = array(
			'name' 				=> _x( 'Home Offices', 'nnn' ),
			'singular_name' 	=> _x( 'Home Office', 'nnn' ),
			'search_items' 		=> __( 'Search Home Offices', 'nnn' ),
			'all_items' 		=> __( 'All Home Offices', 'nnn' ),
			'edit_item' 		=> __( 'Edit Home Office', 'nnn' ),
			'update_item' 		=> __( 'Update Home Office', 'nnn' ),
			'add_new_item' 		=> __( 'Add New Home Office', 'nnn' ),
			'new_item_name' 	=> __( 'New Home Office', 'nnn' ),
			'menu_name' 		=> __( 'Home Offices', 'nnn' ),
			'parent_item'		=> NULL, // remove parent items to prevent breaking if someone adds one
		);

		$args = array(
			'hierarchical' 	    => true,
			'labels' 	    	=> $labels,
			'show_ui' 	    	=> true,
			'show_admin_column' => true,
			'query_var'	    	=> true,
			'rewrite'			=> array( 'slug' => 'locations' ),
		);
		register_taxonomy( 'offices', 'addl_areas', $args );
	}

	function register_post_types() {
		include 'inc/post-type-pest.php';
		include 'inc/post-type-video.php';
		include 'inc/post-type-addl_areas.php';
	}

	// registers and renders our custom acf blocks
	function render_custom_acf_blocks() {
		require 'acf-block-functions.php';
	}

	// custom category for ACF blocks / put category at top of Block Inserter
	function apm_block_category( $categories ) {
		// setup category array
		$apm_category = [
			'slug' => 'apm-blocks',
			'title' => 'APM Blocks'
		];

		// make a new category array and insert ours at position 1
		$new_categories = [];
		$new_categories[0] = $apm_category;

		// rebuild cats array
		foreach( $categories as $category ) {
			$new_categories[] = $category;
		}

		return $new_categories;
	}

	function p2p_init() {
		p2p_register_connection_type( array(
			'name' => 'videos_to_pests',
			'from' => 'video',
			'to' => 'pest',
			'admin_column' => 'from',
			'admin_box' => array(
				'show' => 'from'
			),
			'from_labels' => array(
				'create' => "Add Pest"
			)
		) );
	}

	function dashboard_glance_items( $items ) {
		foreach ( get_post_types(array('public'=>true)) as $post_type ){
			$num_posts = wp_count_posts( $post_type );
			if ( $num_posts && $num_posts->publish ) {
				if ( 'post' == $post_type ) {
					continue;
				}
				if ( 'page' == $post_type ) {
					continue;
				}
				$post_type_object = get_post_type_object( $post_type );
				$text = _n( '%s ' . $post_type_object->labels->singular_name, '%s ' . $post_type_object->label, $num_posts->publish );
				$text = sprintf( $text, number_format_i18n( $num_posts->publish ) );
				if ( $post_type_object && current_user_can( $post_type_object->cap->edit_posts ) ) {
					$items[] = sprintf( '<a href="edit.php?post_type=%1$s">%2$s</a>', $post_type, $text );
				} else {
					$items[] = sprintf( '<span>%2$s</span>', $post_type, $text );
				}
			}
		}
		return $items;
	}
}

new TwentyFourSevenSite();

function apm_render_primary_menu() {
	wp_nav_menu( array(
		'theme_location' => 'primary',
		'container' => '',
		'menu_id' => 'primary-menu'
	));
}

function apm_render_secondary_menu() {
	wp_nav_menu( array(
		'theme_location' => 'secondary',
		'container' => '',
	));
}

function apm_render_quick_links_menu() {
	wp_nav_menu( array(
		'theme_location' => 'quick_links',
		'container' => '',
	));
}

function apm_render_faq_menu() {
	wp_nav_menu( array(
		'theme_location' => 'faq',
		'container' => '',
	));
}

add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

// disable the block editor on certain page templates
function ea_disable_editor( $id = false ) {
	$excluded_templates = [
		'front-page.php',
		'contact.php',
		'about-us.php',
		'locations.php'
	];

	if( empty( $id ) )
		return false;

	$id = intval( $id );
	$template = get_page_template_slug( $id );

	return in_array( $template, $excluded_templates );
}

function ea_disable_gutenberg( $can_edit, $post_type ) {
	if( ! ( is_admin() && !empty( $_GET['post'] ) ) )
		return $can_edit;

	if( ea_disable_editor( $_GET['post'] ) )
		$can_edit = false;

	return $can_edit;
}
add_filter( 'gutenberg_can_edit_post_type', 'ea_disable_gutenberg', 10, 2 );
add_filter( 'use_block_editor_for_post_type', 'ea_disable_gutenberg', 10, 2 );

// APM Facebook Reviews (Call this where needed - Cached for 5 Hours)
// <reviews_ref_id> = 0b3cf92a2d0a27f23e9e93e36e5a3d11ea08a047
// <access_token> = ODM4ZDA0ZjZh4N2M5yMfg4356MDA2NzNlMTRjO5DVhMDQxOTg2MGZhYTQ1MTU4YzJhOWI1YjNmNzVlMTcwNTc5Yw
function get_fb_reviews() {

	// setup our headers with the request so we can handoff our oAuth token and get data back
	$headers = array(
		'Content-Type' => 'application/json',
		'Authorization' => 'Bearer ODM4ZDA0ZjZh4N2M5yMfg4356MDA2NzNlMTRjO5DVhMDQxOTg2MGZhYTQ1MTU4YzJhOWI1YjNmNzVlMTcwNTc5Yw'
	);

	$fb_posts = get_transient( 'remote_posts' );
	if( empty($fb_posts) ) {

		$response = wp_remote_get( 'https://embedsocial.com/admin/v2/api/reviews?reviews_ref=0b3cf92a2d0a27f23e9e93e36e5a3d11ea08a047&page=1&per_page=3', array(
			'headers' => $headers
		));

		if( is_wp_error($response) ) {
			return array();
		}

		$fb_posts = json_decode( wp_remote_retrieve_body($response), true );

		if( empty($fb_posts) ) {
			return array();
		}

		set_transient( 'remote_posts', $fb_posts, 5 * HOUR_IN_SECONDS );
	}

	// Reference for JSON Array Structure
	// echo '<pre style="width: 100%; margin: 0 auto;">'; echo var_dump($fb_posts); echo '</pre>';
	
	if( !empty($fb_posts) ) : ?>
		<div class="testimonials-wrapper">
		<?php
			foreach( $fb_posts['reviews'] as $key => $val ) :
				$reviews = $fb_posts['reviews'][$key];

				// trim the reviews so the cards don't become super tall
				$trimmed_review = wp_trim_words( $reviews['reviewText'], 36, '...' );
				?>
				<div class="testimonials-content">
					<div class="content-head">
						<div>
							<img class="reviewer-image" src="<?= $reviews['reviewerProfilePhotoUrl']; ?>" alt="<?= $reviews['reviewerName']; ?>">
						</div>
						<div>
							<h2><?= $reviews['reviewerName']; ?></h2>
							<div class="review-stars">
								<?php if( $reviews['rating'] === 5 ) : ?>
									<img src="<?= get_stylesheet_directory_uri(); ?>/static/images/icons/review-star.png" alt="Star Rating">
									<img src="<?= get_stylesheet_directory_uri(); ?>/static/images/icons/review-star.png" alt="Star Rating">
									<img src="<?= get_stylesheet_directory_uri(); ?>/static/images/icons/review-star.png" alt="Star Rating">
									<img src="<?= get_stylesheet_directory_uri(); ?>/static/images/icons/review-star.png" alt="Star Rating">
									<img src="<?= get_stylesheet_directory_uri(); ?>/static/images/icons/review-star.png" alt="Star Rating">
								<?php else : ?>
									<img src="<?= get_stylesheet_directory_uri(); ?>/static/images/icons/review-star.png" alt="Star Rating">
									<img src="<?= get_stylesheet_directory_uri(); ?>/static/images/icons/review-star.png" alt="Star Rating">
									<img src="<?= get_stylesheet_directory_uri(); ?>/static/images/icons/review-star.png" alt="Star Rating">
									<img src="<?= get_stylesheet_directory_uri(); ?>/static/images/icons/review-star.png" alt="Star Rating">
									<img src="<?= get_stylesheet_directory_uri(); ?>/static/images/icons/review-star-empty.png" alt="Empty Star Rating">
								<?php endif; ?>
							</div>
						</div>
					</div>

					<div class="content-body">
						<p><?= $trimmed_review; ?></p>
						<a class="trans-button" target="_blank" href="<?= $reviews['pageUrl']; ?>">
							See Review on Facebook <img class="arrow-icon" src="<?= get_stylesheet_directory_uri(); ?>/static/images/icons/arrow-right.svg" alt="Right Arrow"></span>
						</a>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	<?php endif;
}