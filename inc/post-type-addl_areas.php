<?php
$labels = array(
	'name'               => __( 'Additional Areas', 'nnn' ),
	'singular_name'      => __( 'City', 'nnn' ),
	'add_new'            => _x( 'Add New City', 'nnn' ),
	'add_new_item'       => __( 'Add New City', 'nnn' ),
	'edit_item'          => __( 'Edit City', 'nnn' ),
	'new_item'           => __( 'New City', 'nnn' ),
	'view_item'          => __( 'View Cities', 'nnn' ),
	'search_items'       => __( 'Search Cities', 'nnn' ),
	'not_found'          => __( 'No Cities found', 'nnn' ),
	'not_found_in_trash' => __( 'No Cities found in Trash', 'nnn' ),
	'menu_name'          => __( 'Service Areas', 'nnn' ),
);

$args = array(
	'labels'              => $labels,
	'hierarchical'        => true,
	'description'         => '',
	'taxonomies'          => array( 'offices' ),
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => false,
	'show_in_rest'	      => true,
	'menu_position'       => null,
	'menu_icon'           => 'dashicons-admin-multisite',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => true,
	'exclude_from_search' => true,
	'has_archive'         => false,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => array( 'slug' => 'location' ),
	'capability_type'     => 'post',
	'supports'            => array( 'title' ),
);
register_post_type( 'addl_areas', $args );