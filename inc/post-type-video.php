<?php
$labels = array(
	'name'                => __( 'Videos', 'apm' ),
	'singular_name'       => __( 'Video', 'apm' ),
	'add_new'             => _x( 'Add New Video', 'apm', 'apm' ),
	'add_new_item'        => __( 'Add New Video', 'apm' ),
	'edit_item'           => __( 'Edit Video', 'apm' ),
	'new_item'            => __( 'New Video', 'apm' ),
	'view_item'           => __( 'View Video', 'apm' ),
	'search_items'        => __( 'Search Videos', 'apm' ),
	'not_found'           => __( 'No Videos found', 'apm' ),
	'not_found_in_trash'  => __( 'No Videos found in Trash', 'apm' ),
	'parent_item_colon'   => __( 'Parent Video:', 'apm' ),
	'menu_name'           => __( 'Video Gallery', 'apm' ),
);

$args = array(
	'labels'              => $labels,
	'hierarchical'        => false,
	'description'         => 'description',
	'taxonomies'          => array(),
	'public'              => false,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => true,
	'menu_position'       => null,
	'menu_icon'           => 'dashicons-format-video',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => true,
	'exclude_from_search' => false,
	'has_archive'         => true,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => array( 'title' )
);

register_post_type( 'video', $args );