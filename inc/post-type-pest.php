<?php
$labels = array(
	'name'                => __( 'Pests', 'apm' ),
	'singular_name'       => __( 'Pest', 'apm' ),
	'add_new'             => _x( 'Add New Pest', 'apm', 'apm' ),
	'add_new_item'        => __( 'Add New Pest', 'apm' ),
	'edit_item'           => __( 'Edit Pest', 'apm' ),
	'new_item'            => __( 'New Pest', 'apm' ),
	'view_item'           => __( 'View Pest', 'apm' ),
	'search_items'        => __( 'Search Pests', 'apm' ),
	'not_found'           => __( 'No Pests found', 'apm' ),
	'not_found_in_trash'  => __( 'No Pests found in Trash', 'apm' ),
	'parent_item_colon'   => __( 'Parent Pest:', 'apm' ),
	'menu_name'           => __( 'Pest Library', 'apm' ),
);

$args = array(
	'labels'              => $labels,
	'hierarchical'        => false,
	'description'         => 'description',
	'taxonomies'          => array(),
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => true,
	'menu_position'       => null,
	'menu_icon'           => 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/PjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJDYXBhXzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IiB3aWR0aD0iNDU2LjgyOHB4IiBoZWlnaHQ9IjQ1Ni44MjhweCIgdmlld0JveD0iMCAwIDQ1Ni44MjggNDU2LjgyOCIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgNDU2LjgyOCA0NTYuODI4OyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+PGc+PGc+PHBhdGggZD0iTTQ1MS4zODMsMjQ3LjU0Yy0zLjYwNi0zLjYxNy03Ljg5OC01LjQyNy0xMi44NDctNS40MjdoLTYzLjk1M3YtODMuOTM5bDQ5LjM5Ni00OS4zOTRjMy42MTQtMy42MTUsNS40MjgtNy44OTgsNS40MjgtMTIuODVjMC00Ljk0Ny0xLjgxMy05LjIyOS01LjQyOC0xMi44NDdjLTMuNjE0LTMuNjE2LTcuODk4LTUuNDI0LTEyLjg0Ny01LjQyNHMtOS4yMzMsMS44MDktMTIuODQ3LDUuNDI0bC00OS4zOTYsNDkuMzk0SDEwNy45MjNMNTguNTI5LDgzLjA4M2MtMy42MTctMy42MTYtNy44OTgtNS40MjQtMTIuODQ3LTUuNDI0Yy00Ljk1MiwwLTkuMjMzLDEuODA5LTEyLjg1LDUuNDI0Yy0zLjYxNywzLjYxNy01LjQyNCw3LjktNS40MjQsMTIuODQ3YzAsNC45NTIsMS44MDcsOS4yMzUsNS40MjQsMTIuODVsNDkuMzk0LDQ5LjM5NHY4My45MzlIMTguMjczYy00Ljk0OSwwLTkuMjMxLDEuODEtMTIuODQ3LDUuNDI3QzEuODA5LDI1MS4xNTQsMCwyNTUuNDQyLDAsMjYwLjM4N2MwLDQuOTQ5LDEuODA5LDkuMjM3LDUuNDI2LDEyLjg0OGMzLjYxNiwzLjYxNyw3Ljg5OCw1LjQzMSwxMi44NDcsNS40MzFoNjMuOTUzYzAsMzAuNDQ3LDUuNTIyLDU2LjUzLDE2LjU2LDc4LjIyNGwtNTcuNjcsNjQuODA5Yy0zLjIzNywzLjgxLTQuNzEyLDguMjM0LTQuNDI1LDEzLjI3NWMwLjI4NCw1LjAzNywyLjIzNSw5LjI3Myw1Ljg1MiwxMi43MDNjMy42MTcsMy4wNDUsNy43MDcsNC41NzEsMTIuMjc1LDQuNTcxYzUuMzMsMCw5Ljg5Ny0xLjk5MSwxMy43MDYtNS45OTVsNTIuMjQ2LTU5LjEwMmw0LjI4NSw0LjAwNGMyLjY2NCwyLjQ3OSw2LjgwMSw1LjU2NCwxMi40MTksOS4yNzRjNS42MTcsMy43MSwxMS44OTcsNy40MjMsMTguODQyLDExLjE0M2M2Ljk1LDMuNzEsMTUuMjMsNi44NTIsMjQuODQsOS40MThjOS42MTQsMi41NzMsMTkuMjczLDMuODYsMjguOTgsMy44NlYxNjkuMDM0aDM2LjU0N1Y0MjQuODVjOS4xMzQsMCwxOC4zNjMtMS4yMzksMjcuNjg4LTMuNzE3YzkuMzI4LTIuNDcxLDE3LjEzNS01LjIzMiwyMy40MTgtOC4yNzhjNi4yNzUtMy4wNDksMTIuNDctNi41MTksMTguNTU1LTEwLjQyYzYuMDkyLTMuOTAxLDEwLjA4OS02LjYxMiwxMS45OTEtOC4xMzhjMS45MDktMS41MjYsMy4zMzMtMi43NjIsNC4yODQtMy43MWw1Ni41MzQsNTYuMjQzYzMuNDMzLDMuNjE3LDcuNzA3LDUuNDI0LDEyLjg0Nyw1LjQyNGM1LjE0MSwwLDkuNDIyLTEuODA3LDEyLjg1NC01LjQyNGMzLjYwNy0zLjYxNyw1LjQyMS03LjkwMiw1LjQyMS0xMi44NTFzLTEuODEzLTkuMjMyLTUuNDIxLTEyLjg0N2wtNTkuMzg4LTU5LjY2OWMxMi43NTUtMjIuNjUxLDE5LjEzLTUwLjI1MSwxOS4xMy04Mi43OTZoNjMuOTUzYzQuOTQ5LDAsOS4yMzYtMS44MSwxMi44NDctNS40MjdjMy42MTQtMy42MTQsNS40MzItNy44OTgsNS40MzItMTIuODQ3QzQ1Ni44MjgsMjU1LjQ0NSw0NTUuMDExLDI1MS4xNTgsNDUxLjM4MywyNDcuNTR6Ii8+PHBhdGggZD0iTTI5My4wODEsMzEuMjdjLTE3Ljc5NS0xNy43OTUtMzkuMzUyLTI2LjY5Ni02NC42NjctMjYuNjk2Yy0yNS4zMTksMC00Ni44Nyw4LjkwMS02NC42NjgsMjYuNjk2Yy0xNy43OTUsMTcuNzk3LTI2LjY5MSwzOS4zNTMtMjYuNjkxLDY0LjY2N2gxODIuNzE2QzMxOS43NzEsNzAuNjI3LDMxMC44NzYsNDkuMDY3LDI5My4wODEsMzEuMjd6Ii8+PC9nPjwvZz48Zz48L2c+PGc+PC9nPjxnPjwvZz48Zz48L2c+PGc+PC9nPjxnPjwvZz48Zz48L2c+PGc+PC9nPjxnPjwvZz48Zz48L2c+PGc+PC9nPjxnPjwvZz48Zz48L2c+PGc+PC9nPjxnPjwvZz48L3N2Zz4=',
	'show_in_nav_menus'   => true,
	'publicly_queryable'  => true,
	'exclude_from_search' => false,
	'has_archive'         => true,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => array( 'slug' => 'pests' ),
	'capability_type'     => 'post',
	'supports'            => array( 'title', 'editor', 'thumbnail', 'excerpt', 'revisions' )
);

register_post_type( 'pest', $args );