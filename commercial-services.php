<?php
/**
 * Template Name: Commercial Services Landing
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$templates = array( 'commercial-services.twig' );
Timber::render( $templates, $context );