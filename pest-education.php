<?php
/**
 * Template Name: Pest Education
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$context['videos'] = Timber::get_posts( array( 'post_type' => 'video', 'posts_per_page' => 1000 ) );
the_post();

$templates = array( 'pest-education.twig' );

Timber::render( $templates, $context );